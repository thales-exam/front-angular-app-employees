import { HttpHeaders } from "@angular/common/http";

export class Util {

    public static RESOURCES_ENDPOINT = 'http://localhost:8082';

    public static MAIN_ENPOINT = 'http://localhost:8082/api';

    public static HTTP_HEADERS = new HttpHeaders({
        'Content-Type': 'application/json'
    });
}