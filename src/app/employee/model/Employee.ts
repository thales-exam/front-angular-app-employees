export class Employee {
  id: number;
  name: string;
  salary: number;
  age: number;
  profileImage: string;
  anualSalary: number;
}
