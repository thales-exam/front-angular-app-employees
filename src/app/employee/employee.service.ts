import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Employee } from './model/Employee';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Util } from '../utils/util';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  getEmployees(): Observable<any> {
    return this.http.get<Employee[]>(Util.MAIN_ENPOINT+'/employees');
  }

  getEmployeeById(id:number){
    return this.http.get<Employee>(Util.MAIN_ENPOINT+'/employees/'+id)
      .pipe(catchError(e=> {return throwError(e);}));
  }

  create(employee:Employee): Observable<Employee>{
    return this.http.post<Employee>(Util.MAIN_ENPOINT+'/employees', employee, {headers: Util.HTTP_HEADERS});
  }

  update(employee: Employee): Observable<Employee>{
    return this.http.put<Employee>(Util.MAIN_ENPOINT+'/employees/'+employee.id, employee, {headers: Util.HTTP_HEADERS});
  }

  delete(id:number): Observable<any>{
    return this.http.delete<any>(Util.MAIN_ENPOINT+'/employees/'+id);
  }
}
