import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from './employee.service';
import { Employee as Employee } from './model/Employee';
import { Util } from '../utils/util';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  @ViewChild('employeeModal') employeeModal: any;

  //@ViewChild('search') search: ElementRef;

  public showModal = false;

  public employees: Employee[];

  public employee: Employee = new Employee();

  public modaltitle: string;

  public modalButtonText: String;

  public defaultProfileImage: string;

  constructor(
    private employeeService: EmployeeService,
    private search: ElementRef
  ) {
    this.search.nativeElement.querySelector('#search');
  }

  ngOnInit(): void {
    this.defaultProfileImage = Util.RESOURCES_ENDPOINT + '/images/no-user.png';
    this.refreshElements();
  }

  async showLoading(){
    await Swal.fire({
      title: 'loading data!',
      text: 'please wait a moment while data is loaded',
      didOpen: () => {
        Swal.showLoading()
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer')
      }
    })
  }

  public refreshElements(): void {
    this.showLoading();
    //this.spinnerService.show();
    this.search.nativeElement.querySelector('#search').value = '';
    this.employee = new Employee();
    this.employeeService.getEmployees().subscribe((employees) => {
      //this.spinnerService.hide();
      this.employees = employees;
      Swal.close();


      if (!this.employees?.length) {
        Swal.fire({
          title: 'Error dummy data couln´t be loaded!',
          text: 'Somethimes dummy fails, please click on Reload button',
          icon: 'error',
          confirmButtonText: 'Done',
        });
      }
    });
  }

  findOne() {
    this.showLoading();
    //this.spinnerService.show();
    var idToFind = this.search.nativeElement.querySelector('#search').value;
    if (idToFind === null || idToFind === undefined || idToFind === '') {
      this.refreshElements();
      return;
    }
    this.employeeService.getEmployeeById(idToFind).subscribe(
      (employee) => {
        //this.spinnerService.hide();
        this.employees = [];
        this.employees.push(employee);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Employee exists',
          showConfirmButton: false,
          timer: 1000,
        });
      },
      (error) => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title:
            'Error Employee does not exists or dummy database has some error! ',
          text: 'all data will be loaded again',
          confirmButtonText: 'Ok',
        }).then((result) => {
          this.refreshElements();
        });
        console.error(error);
        //this.spinnerService.hide();
      }
    );
  }

  public openCreateDialog() {
    this.modalButtonText = 'Create';
    this.modaltitle = 'Create new element';
    this.employee = new Employee();
  }

  public openUpdateDialog(employee: Employee) {
    this.modalButtonText = 'View';
    this.employee = employee;
    this.modaltitle = 'Employee: ' + employee.id + ' - ' + employee.name;
  }

  public createUpdate(): void {
    if (this.modalButtonText === 'Create') {
      this.employeeService
        .create(this.employee)
        .subscribe((reponse) => this.refreshElements());
    } else if (this.modalButtonText === 'Update') {
      this.employeeService
        .update(this.employee)
        .subscribe((reponse) => this.refreshElements());
    }
  }

  public openDeleteDialog(employee: Employee): void {
    this.modaltitle = 'Delete element';
    this.employee = employee;
  }

  public deleteElement(id: number): void {
    this.employeeService
      .delete(id)
      .subscribe((reponse) => this.refreshElements());
  }
}
